package com.multichannel.chatcustomer.ui.activity;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.core.util.Pair;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.multichannel.chatcustomer.R;
import com.multichannel.chatcustomer.database.SharedPref;
import com.multichannel.chatcustomer.helper.QiscusChatScrollListener;
import com.multichannel.chatcustomer.helper.QiscusCustomEditText;
import com.multichannel.chatcustomer.presenter.imp.RoomChatPresenterImp;
import com.multichannel.chatcustomer.ui.adapter.RoomChatAdapter;
import com.multichannel.chatcustomer.ui.adapter.viewholder.CardViewHolder;
import com.multichannel.chatcustomer.ui.adapter.viewholder.CarouselItemView;
import com.multichannel.chatcustomer.ui.adapter.viewholder.ChatButtonView;
import com.multichannel.chatcustomer.ui.view.RoomChatView;
import com.multichannel.chatcustomer.util.Constant;
import com.multichannel.chatcustomer.util.KeyboardUtil;
import com.multichannel.chatcustomer.util.PermissionUtil;
import com.multichannel.chatcustomer.util.QiscusImageUtil;
import com.multichannel.chatcustomer.util.QiscusPermissionsUtil;
import com.qiscus.jupuk.JupukBuilder;
import com.qiscus.jupuk.JupukConst;
import com.qiscus.nirmana.Nirmana;
import com.qiscus.sdk.chat.core.QiscusCore;
import com.qiscus.sdk.chat.core.data.model.QiscusChatRoom;
import com.qiscus.sdk.chat.core.data.model.QiscusComment;
import com.qiscus.sdk.chat.core.data.model.QiscusRoomMember;
import com.qiscus.sdk.chat.core.util.QiscusAndroidUtil;
import com.qiscus.sdk.chat.core.util.QiscusDateUtil;
import com.qiscus.sdk.chat.core.util.QiscusFileUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import rx.Observable;

public class RoomChatActivity extends AppCompatActivity implements QiscusChatScrollListener.Listener,
        RoomChatView, ActionMode.Callback, CommentSelectedListener {
    protected static final int TAKE_PICTURE_REQUEST = 3;
    protected static final int RC_CAMERA_PERMISSION = 128;
    protected static final int SET_TEXT_FOR_IMAGE = 137;
    private static final String[] FILE_PERMISSION = {
            "android.permission.WRITE_EXTERNAL_STORAGE",
            "android.permission.READ_EXTERNAL_STORAGE"
    };
    private static final String[] CAMERA_PERMISSION = {
            "android.permission.CAMERA",
            "android.permission.WRITE_EXTERNAL_STORAGE",
            "android.permission.READ_EXTERNAL_STORAGE",
    };
    private static final int REQUEST_PICK_IMAGE = 1;
    private static final int REQUEST_FILE_PERMISSION = 2;
    String userName = "";
    String userEmail = "";
    String userPhone = "";
    long roomId;
    long afterTextChangedDelay = 3000;
    Handler handler = new Handler();
    String imageFilePath;
    String adminUsername = "";
    String typingText = " is Typing";
    private Toolbar mToolbar;
    private RecyclerView mRecData;
    private TextView mTextToolbarTitle;
    private TextView mTextNewChatNotif;
    private TextView mTextParticipant;
    private TextView mTextRepliedUser;
    private TextView mTextRepliedMessage;
    private TextView mTextStartChat;
    private ImageView mImageBack;
    private CircleImageView mImageAvatar;
    private ImageView mImageSend;
    private ImageView mImageRepliedMessage;
    private ProgressBar mProgressBar;
    private RelativeLayout mLayoutImageAttachment;
    private RelativeLayout mLayoutRepliedMessage;
    private LinearLayout mLayoutAttachment;
    private LinearLayout mAttCamera;
    private LinearLayout mAttGallery;
    private LinearLayout mAttFile;
    private LinearLayout mAttCancel;
    private RelativeLayout mLayoutMessage;
    private QiscusCustomEditText mEditComment;
    private View mViewMessageBox;
    private RoomChatAdapter adapter;
    private LinearLayoutManager layoutManager;
    private ChatButtonView.ChatButtonClickListener chatButtonClickListener;
    private CarouselItemView.ChatItemClickListener itemClickListener;
    private CardViewHolder.ChatItemClickListener cardItemClickListener;
    private CommentSelectedListener commentSelectedListener;
    private RoomChatPresenterImp presenter;
    private int newMessageCount = 0;

    private QiscusComment repliedQiscusComment;
    private Map<String, QiscusRoomMember> roomMembers;
    private boolean isMultichannel;
    private boolean isResolved = false;
    private QiscusComment userCommentWhenRoomResolved = null;

    private ActionMode actionMode;
    private Runnable input_finish_checker = () -> {
        presenter.setUserTyping(false);
    };

    private SharedPref sharedPref;
    private Runnable commentHighlightTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_chat);

//        checkPermissions();
        findView();
        init();
//        presenter = new RoomChatPresenterImp(this);

    }

    private void findView() {
        mToolbar = findViewById(R.id.toolbar);
        mRecData = findViewById(R.id.rv_data);
        mTextToolbarTitle = findViewById(R.id.tv_toolbar_title);
        mTextNewChatNotif = findViewById(R.id.tv_new_chat_notif);
        mTextParticipant = findViewById(R.id.tv_participant);
        mTextRepliedUser = findViewById(R.id.tv_replied_username);
        mTextRepliedMessage = findViewById(R.id.tv_replied_message);
        mTextStartChat = findViewById(R.id.tv_start_chat);
        mImageBack = findViewById(R.id.iv_back);
        mImageAvatar = findViewById(R.id.iv_avatar);
        mImageSend = findViewById(R.id.iv_send);
        mImageRepliedMessage = findViewById(R.id.iv_replied_image);
        mProgressBar = findViewById(R.id.progress_bar);
        mEditComment = findViewById(R.id.field_message);
        mViewMessageBox = findViewById(R.id.shadow_message_box);

        mLayoutImageAttachment = findViewById(R.id.layout_iv_attachment);
        mLayoutRepliedMessage = findViewById(R.id.layout_reply);
        mLayoutAttachment = findViewById(R.id.layout_attachment);
        mLayoutMessage = findViewById(R.id.layout_message);
        mAttCamera = findViewById(R.id.att_camera);
        mAttGallery = findViewById(R.id.att_gallery);
        mAttFile = findViewById(R.id.att_file);
        mAttCancel = findViewById(R.id.att_cancel);
    }

    private void init() {
        setSupportActionBar(mToolbar);

        setClickButtonView();

        setViewEnabled(mImageSend, false);
        changeImageSendTint(mImageSend, R.color.qiscus_disabled);

        userCommentWhenRoomResolved = null;

        layoutManager = new LinearLayoutManager(this);
        layoutManager.setReverseLayout(true);
        mRecData.setLayoutManager(layoutManager);
        mRecData.setHasFixedSize(true);
        mRecData.addOnScrollListener(new QiscusChatScrollListener(layoutManager, this));
        adapter = new RoomChatAdapter(this, chatButtonClickListener, itemClickListener, cardItemClickListener);

        mRecData.setAdapter(adapter);

        presenter = new RoomChatPresenterImp(this);

        sharedPref = new SharedPref(this);

        Intent intent = getIntent();

        if (intent != null) {
            roomId = intent.getLongExtra("roomId", 0);
            if (roomId != 0) {
                isMultichannel = intent.getBooleanExtra("multichannel", false);
                if (!isMultichannel) {
                    presenter.callRoomData(roomId);
                } else {
                    isResolved = intent.getBooleanExtra("isResolved", false);
                    if (isResolved) {
                        presenter.callRoomData(roomId);
                    } else {
                        presenter.initiateChat(QiscusCore.getQiscusAccount().getEmail(), QiscusCore.getQiscusAccount().getUsername(),QiscusCore.getQiscusAccount().getExtras().toString());
                    }
                }
            } else {
                isMultichannel = true;
                userName = intent.getStringExtra("name");
                userEmail = intent.getStringExtra("email");
                userPhone = intent.getStringExtra("phone");
                presenter.initiateChat(userEmail, userName,userPhone);
            }
        }

        commentSelectedListener = this::onCommentSelected;

        initClickListener();

        initTypingListener();
    }

    private void initTypingListener() {
        mEditComment.setOnKeyboardListener((keyboardEditText, showing) -> {
            if (!showing) {
                presenter.setUserTyping(false);
            }
        });

        mEditComment.addTextChangedListener(new textWatcher());
    }

    private void setClickButtonView() {
        itemClickListener = jsonButton -> {
            String text = jsonButton.optString("postback_text");
            presenter.generateComment(text, "text", isResolved);

        };

        cardItemClickListener = jsonButton -> {
            String text = jsonButton.optString("postback_text");
            presenter.generateComment(text, "text", isResolved);
        };

        chatButtonClickListener = jsonButton -> {
            try {
                JSONObject jsonObject = new JSONObject(jsonButton.optString("payload"));
                String postBack = jsonButton.optString("postback_text");
                presenter.generateButtonComment(jsonButton.optString("payload"), postBack, "button_postback_response");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        };
    }

    private void initClickListener() {
        mImageBack.setOnClickListener(v -> {
//            Intent i = new Intent(this, RoomListActivity.class);
//            i.putExtra(Constant.ACOUNT_DATA, QiscusCore.getQiscusAccount());
//            i.putExtra("name", QiscusCore.getQiscusAccount().getUsername());
//            i.putExtra("email", QiscusCore.getQiscusAccount().getEmail());
//            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            startActivity(i);
//            overridePendingTransition(0, 0);
//
            finish();
        });

        mLayoutImageAttachment.setOnClickListener(v -> {
            if (mLayoutAttachment.getVisibility() == View.GONE) {
                mLayoutAttachment.setVisibility(View.VISIBLE);
            } else {
                mLayoutAttachment.setVisibility(View.GONE);
            }
        });

        mImageSend.setOnClickListener(v -> {
            sendComment();
        });

        mAttCamera.setOnClickListener(v -> {
            if (QiscusPermissionsUtil.hasPermissions(RoomChatActivity.this, CAMERA_PERMISSION)) {
                openCamera();
                mLayoutAttachment.setVisibility(View.GONE);
            } else {
                requestCameraPermission();
            }
        });

        mAttGallery.setOnClickListener(v -> {
            if (QiscusPermissionsUtil.hasPermissions(RoomChatActivity.this, FILE_PERMISSION)) {
                pickImage();
                mLayoutAttachment.setVisibility(View.GONE);
            } else {
                requestFilePermission();
            }
        });

        mAttFile.setOnClickListener(v -> {
            if (QiscusPermissionsUtil.hasPermissions(RoomChatActivity.this, FILE_PERMISSION)) {
                pickFile();
                mLayoutAttachment.setVisibility(View.GONE);
            } else {
                requestFilePermission();
            }
        });

        mAttCancel.setOnClickListener(v ->
                mLayoutAttachment.setVisibility(View.GONE));

        mRecData.setOnClickListener(v -> {
            if (mLayoutAttachment.getVisibility() == View.VISIBLE) {
                mLayoutAttachment.setVisibility(View.GONE);
            }
        });

        mTextNewChatNotif.setOnClickListener(v -> {
            mRecData.smoothScrollToPosition(0);
            newMessageCount = 0;
            mTextNewChatNotif.setVisibility(View.GONE);
        });

        adapter.setOnLongItemClickListener((view, position) -> onItemCommentLongClick(adapter.getData().get(position)));

        adapter.setOnItemClickListener((view, position) -> onItemCommentClick(adapter.getData().get(position)));

        adapter.setOnReplyItemClickListener(comment -> scrollToComment(comment.getReplyTo()));

        mTextStartChat.setOnClickListener(v -> {
            Intent intent = getIntent();
            intent.putExtra("roomId", Long.valueOf(0));
            intent.putExtra("name", QiscusCore.getQiscusAccount().getUsername());
            intent.putExtra("email", QiscusCore.getQiscusAccount().getEmail());
            startActivity(intent);
            finish();
        });
    }

    private void scrollToComment(QiscusComment qiscusComment) {
        int position = adapter.findPosition(qiscusComment);
        if (position >= 0) {
            mRecData.scrollToPosition(position);
            highlightComment(adapter.getData().get(position));
        } else {
            presenter.loadUntilComment(qiscusComment);
        }
    }

    protected void highlightComment(QiscusComment qiscusComment) {
        qiscusComment.setHighlighted(true);
        adapter.notifyDataSetChanged();
        commentHighlightTask = () -> {
            qiscusComment.setHighlighted(false);
            adapter.notifyDataSetChanged();
        };
        QiscusAndroidUtil.runOnUIThread(commentHighlightTask, 2000);
    }

    protected void onItemCommentClick(QiscusComment qiscusComment) {
        if (adapter.getSelectedComments().isEmpty()) {
            if (qiscusComment.getType() == QiscusComment.Type.TEXT
                    || qiscusComment.getType() == QiscusComment.Type.LINK
                    || qiscusComment.getType() == QiscusComment.Type.IMAGE
                    || qiscusComment.getType() == QiscusComment.Type.AUDIO
                    || qiscusComment.getType() == QiscusComment.Type.VIDEO
                    || qiscusComment.getType() == QiscusComment.Type.FILE
                    || qiscusComment.getType() == QiscusComment.Type.REPLY
                    || qiscusComment.getType() == QiscusComment.Type.CONTACT
                    || qiscusComment.getType() == QiscusComment.Type.LOCATION) {
                toggleSelectComment(qiscusComment);
            }
        }
    }

    protected void onItemCommentLongClick(QiscusComment qiscusComment) {
        if (adapter.getSelectedComments().isEmpty()
                && (qiscusComment.getType() == QiscusComment.Type.TEXT
                || qiscusComment.getType() == QiscusComment.Type.LINK
                || qiscusComment.getType() == QiscusComment.Type.IMAGE
                || qiscusComment.getType() == QiscusComment.Type.AUDIO
                || qiscusComment.getType() == QiscusComment.Type.VIDEO
                || qiscusComment.getType() == QiscusComment.Type.FILE
                || qiscusComment.getType() == QiscusComment.Type.REPLY
                || qiscusComment.getType() == QiscusComment.Type.CONTACT
                || qiscusComment.getType() == QiscusComment.Type.LOCATION)) {
            toggleSelectComment(qiscusComment);
        }
    }

    protected void toggleSelectComment(QiscusComment qiscusComment) {
        qiscusComment.setSelected(!qiscusComment.isSelected());
        adapter.notifyDataSetChanged();
        if (commentSelectedListener != null) {
            commentSelectedListener.onCommentSelected(adapter.getSelectedComments());
        }
    }

    public void sendComment() {
        presenter.setUserTyping(false);
        String comment = mEditComment.getText().toString();
        if (mLayoutRepliedMessage.getVisibility() == View.VISIBLE) {
            if (repliedQiscusComment != null) {
                presenter.generateReplyComment(repliedQiscusComment, comment, isResolved);
            }
            mLayoutRepliedMessage.setVisibility(View.GONE);
        } else {
            presenter.generateComment(comment, "text", isResolved);
        }

        mEditComment.setText("");

    }

    public void hideRepliedMessageLayout(View view) {
        mLayoutRepliedMessage.setVisibility(View.GONE);
    }

    private void setViewEnabled(View view, boolean enabled) {
        view.setEnabled(enabled);
        view.setClickable(enabled);
        view.setFocusable(enabled);
    }

    private void changeImageSendTint(ImageView imageView, int color) {
        imageView.setColorFilter(getResources().getColor(color), PorterDuff.Mode.SRC_ATOP);
    }

    private void pickFile() {
        JupukBuilder builder = new JupukBuilder();
        builder.setMaxCount(1)
                .setColorPrimary(ContextCompat.getColor(this, R.color.colorPrimary))
                .setColorPrimaryDark(ContextCompat.getColor(this, R.color.colorPrimaryDark))
                .setColorAccent(ContextCompat.getColor(this, R.color.colorAccent))
                .pickDoc(RoomChatActivity.this);
    }

    private void openCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(getApplication().getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = QiscusImageUtil.createImageFile();
                imageFilePath = photoFile.getAbsolutePath();
            } catch (IOException ex) {
                Toast.makeText(getApplication(), "Failed to write temporary picture!", Toast.LENGTH_SHORT).show();
            }

            if (photoFile != null) {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                } else {
                    intent.putExtra(MediaStore.EXTRA_OUTPUT,
                            FileProvider.getUriForFile(getApplication(), sharedPref.getProviderAuthorities(), photoFile));
                }
                startActivityForResult(intent, TAKE_PICTURE_REQUEST);
            }
            mLayoutAttachment.setVisibility(View.GONE);
        } else {
            Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();
        }
    }

    private void pickImage() {
        new JupukBuilder().setMaxCount(10)
                .setColorPrimary(ContextCompat.getColor(this, R.color.colorPrimary))
                .setColorPrimaryDark(ContextCompat.getColor(this, R.color.colorPrimaryDark))
                .setColorAccent(ContextCompat.getColor(this, R.color.colorAccent))
                .pickPhoto(this);
    }

    private void requestCameraPermission() {
        if (!QiscusPermissionsUtil.hasPermissions(this, CAMERA_PERMISSION)) {
            QiscusPermissionsUtil.requestPermissions(this, getString(R.string.qiscus_permission_request_title),
                    RC_CAMERA_PERMISSION, CAMERA_PERMISSION);
        }
    }

    private void requestFilePermission() {
        if (!QiscusPermissionsUtil.hasPermissions(this, FILE_PERMISSION)) {
            QiscusPermissionsUtil.requestPermissions(this, getString(R.string.qiscus_permission_request_title),
                    REQUEST_FILE_PERMISSION, FILE_PERMISSION);
        }
    }

    private void loadMoreComments() {
        if (mProgressBar.getVisibility() == View.GONE && adapter.getItemCount() > 0) {
            QiscusComment comment = adapter.getData().get(adapter.getItemCount() - 1);
            if (comment.getId() == -1 || comment.getCommentBeforeId() > 0) {
                presenter.loadOlderRoomComments(comment);
                mProgressBar.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onTopOffListMessage() {
        loadMoreComments();
    }

    @Override
    public void onMiddleOffListMessage() {
    }

    @Override
    public void onBottomOffListMessage() {
        if (mTextNewChatNotif.getVisibility() == View.VISIBLE) {
            mTextNewChatNotif.setVisibility(View.GONE);
        }

        newMessageCount = 0;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        QiscusPermissionsUtil.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == JupukConst.REQUEST_CODE_PHOTO) {
                String fileJupuk = data.getStringArrayListExtra(JupukConst.KEY_SELECTED_MEDIA).get(0);
                Intent i = new Intent(this, ImageCommentActivity.class);
                i.putExtra(Constant.DATA, "file://" + fileJupuk);
                startActivityForResult(i, SET_TEXT_FOR_IMAGE);
            } else if (requestCode == TAKE_PICTURE_REQUEST) {
                String photoFile = imageFilePath;
                Intent i = new Intent(this, ImageCommentActivity.class);
                i.putExtra(Constant.DATA, "file://" + photoFile);
                startActivityForResult(i, SET_TEXT_FOR_IMAGE);
            } else if (requestCode == SET_TEXT_FOR_IMAGE) {
                File file = getImageOrDocFile(data.getStringExtra("uri"));
                String comment = data.getStringExtra("comment");

                presenter.checkFile(file, comment, "file_attachment", isResolved);

            } else if (requestCode == JupukConst.REQUEST_CODE_DOC) {
                String docFileUri = data.getStringArrayListExtra(JupukConst.KEY_SELECTED_DOCS).get(0);

                presenter.checkFile(new File(docFileUri), "", "file", isResolved);

            }
        }
    }

    private File getImageOrDocFile(String fileUri) {
        File file = null;
        try {
            file = QiscusFileUtil.from(Uri.parse(fileUri));

        } catch (IOException e) {
            Toast.makeText(this, "Failed to open image file!", Toast.LENGTH_SHORT).show();
        }
        return file;
    }

    @Override
    public void setRoomData(Pair<QiscusChatRoom, List<QiscusComment>> qiscusChatRoomListPair) {
        boolean isFromDialog = getIntent().getBooleanExtra("isFromDialog", false);
        if (isFromDialog) {
            QiscusComment qiscusComment = (QiscusComment) getIntent().getSerializableExtra("comment");
            presenter.sendComment(qiscusComment);
            sendingComment(qiscusComment);
        }
        adapter.addOrUpdate(qiscusChatRoomListPair.second);
        String roomName = Observable.from(qiscusChatRoomListPair.first.getMember())
                .map(QiscusRoomMember::getUsername)
                .filter(username -> !username.equals(QiscusCore.getQiscusAccount().getUsername()))
                .first()
                .toBlocking()
                .single();
        mTextToolbarTitle.setText(roomName);
        Glide.with(RoomChatActivity.this)
                .load(qiscusChatRoomListPair.first.getAvatarUrl())
                .into(mImageAvatar);
    }

    @Override
    public void setOlderComment(List<QiscusComment> comments) {
        adapter.addOrUpdate(comments);
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void setUploadProgress(long progress) {
        mProgressBar.setVisibility(View.VISIBLE);
        mProgressBar.setProgress((int) progress);
    }

    @Override
    public void uploadSuccess() {
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void sendingComment(QiscusComment qiscusComment) {
        adapter.addOrUpdate(qiscusComment);
        mRecData.smoothScrollToPosition(0);
    }

    @Override
    public void errorMessage(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNewComment(QiscusComment qiscusComment) {
        if (qiscusComment.getType() == QiscusComment.Type.SYSTEM_EVENT) {
            String eventLastWord = qiscusComment.getMessage().substring(qiscusComment.getMessage().lastIndexOf(" ") + 1);
            if (eventLastWord.equals("resolved")) {
                isResolved = true;
                presenter.sessionalCheck();
            }
        }
        adapter.addOrUpdate(qiscusComment);
        if (!shouldShowNewMessageButton()) {
            mRecData.smoothScrollToPosition(0);
        } else {
            String newChatNotifText = (newMessageCount += 1) + " New Message";
            mTextNewChatNotif.setText(newChatNotifText);
            if (mTextNewChatNotif.getVisibility() == View.GONE) {
                mTextNewChatNotif.setVisibility(View.VISIBLE);
            }
        }
    }

    private boolean shouldShowNewMessageButton() {
        return layoutManager.findFirstVisibleItemPosition() > 2;
    }

    @Override
    public void onCommentSuccess(QiscusComment qiscusComment) {
        adapter.removePendingComment(qiscusComment);
        adapter.addOrUpdate(qiscusComment);

    }

    @Override
    public void showCommentsAndScrollToTop(List<QiscusComment> qiscusComments) {
        if (!qiscusComments.isEmpty()) {
            adapter.addOrUpdate(qiscusComments);
            mRecData.scrollToPosition(adapter.getItemCount() - 1);
            highlightComment(adapter.getData().get(adapter.getItemCount() - 1));
        }
    }

    @Override
    public void onFailedSendComment(QiscusComment qiscusComment) {
        adapter.addOrUpdate(qiscusComment);
    }

    @Override
    public void setRoomResolved(boolean isResolved) {
        this.isResolved = isResolved;
    }

    @Override
    public void setLastCommentWhenRoomResolved(QiscusComment qiscusComment) {
        userCommentWhenRoomResolved = qiscusComment;
    }

    @Override
    public void updateLastDeliveredMessage(long lastDeliveredCommentId) {
        adapter.updateLastDeliveredComment(lastDeliveredCommentId);
    }

    @Override
    public void updateLastReadComment(long lastReadCommentId) {
        adapter.updateLastReadComment(lastReadCommentId);
    }

    @Override
    public void onCommentDeleted(QiscusComment qiscusComment) {
        adapter.remove(qiscusComment);
    }

    @Override
    public void refreshComment(QiscusComment qiscusComment) {
        adapter.addOrUpdate(qiscusComment);
    }

    @Override
    public void onRealtimeStatusChanged(boolean connected) {
        if (connected) {
            QiscusComment comment = adapter.getLatestSentComment();
            if (comment != null) {
                presenter.loadCommentsAfter(comment);
            }
        }
    }

    @Override
    public void onLoadMore(List<QiscusComment> comments) {
        adapter.addOrUpdate(comments);
    }

    @Override
    public void onUserTyping(boolean typing) {
        if (typing) {
            mTextParticipant.setText(typingText);
        } else {
            mTextParticipant.setText(adminUsername);
        }
    }

    @Override
    public void setOnlineStatus(boolean online, Date lastActive) {
        if (isMultichannel) {
            mTextParticipant.setText(adminUsername);
        } else {
            if (online) {
                mTextParticipant.setText("Online");
            } else {
                mTextParticipant.setText("Last seen " + QiscusDateUtil.getRelativeTimeDiff(lastActive));
            }
        }
    }

    @Override
    public void setParticipants(List<QiscusRoomMember> member) {
        adminUsername = Observable.from(member)
                .map(QiscusRoomMember::getUsername)
                .filter(username -> !username.equals(QiscusCore.getQiscusAccount().getUsername()))
                .first()
                .toBlocking()
                .single();

        typingText = adminUsername + " " + getString(R.string.is_typing);
        adminUsername += " dan kamu";
        if (isMultichannel)
            mTextParticipant.setText(adminUsername);
    }

    @Override
    public void dismissLoading() {
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void showError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setMessageBoxForQismo(boolean isSessional) {
        if (isSessional) {
            runOnUiThread(() -> {
                if (userCommentWhenRoomResolved != null) {
                    callDialogForStartChat();
                }
                mLayoutMessage.setVisibility(View.GONE);
                mViewMessageBox.setVisibility(View.VISIBLE);
                mTextStartChat.setVisibility(View.VISIBLE);
            });
        }
        new Handler().postDelayed(() -> presenter.sessionalCheck(), 40000);
    }

    private void callDialogForStartChat() {
        if (!RoomChatActivity.this.isFinishing()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(false);
            builder.setTitle("Terjadi perubahan");
            builder.setMessage("Admin telah mengubah beberapa pengaturan, tekan ok untuk membuka room baru")
                    .setCancelable(false)
                    .setPositiveButton("OK", (dialog, id) -> {
                        //do things
                        dialog.dismiss();
                        Intent intent = getIntent();
                        intent.putExtra("roomId", Long.valueOf(0));
                        intent.putExtra("name", QiscusCore.getQiscusAccount().getUsername());
                        intent.putExtra("email", QiscusCore.getQiscusAccount().getEmail());
                        intent.putExtra("isFromDialog", true);
                        intent.putExtra("comment", userCommentWhenRoomResolved);
                        startActivity(intent);
                        finish();
                        overridePendingTransition(0, 0);
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    private void notifyLatestRead() {
        QiscusComment qiscusComment = adapter.getLatestSentComment();
        presenter.notifyLastRead(qiscusComment);
    }

    @Override
    public void onBackPressed() {
        finish();
//        if (!RoomListActivity.isActive) {
//        Intent i = new Intent(this, RoomListActivity.class);
//        i.putExtra(Constant.ACOUNT_DATA, QiscusCore.getQiscusAccount());
//        i.putExtra("name", QiscusCore.getQiscusAccount().getUsername());
//        i.putExtra("email", QiscusCore.getQiscusAccount().getEmail());
//        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        startActivity(i);
//        overridePendingTransition(0, 0);
//        }
        super.onBackPressed();
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.setUserTyping(false);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (commentHighlightTask != null) {
            QiscusAndroidUtil.cancelRunOnUIThread(commentHighlightTask);
        }
        notifyLatestRead();
        presenter.detachView();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.setLastChat(true);
        notifyLatestRead();
    }

    @Override
    public void onPause() {
        super.onPause();
        presenter.setLastChat(false);
    }

    @Override
    protected void onStart() {
        super.onStart();
        replyButton();
    }

    private void checkPermissions() {
        int PERMISSION_ALL = 101;
        String[] PERMISSIONS = {
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                android.Manifest.permission.CAMERA
        };

        if (!PermissionUtil.hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }
    }

    private void replyButton() {
        new ChatButtonView.ChatButtonClickListener() {
            @Override
            public void onChatButtonClick(JSONObject jsonButton) {
                try {
                    JSONObject jsonObject = new JSONObject().getJSONObject(jsonButton.optString("payload"));
                    String postBack = jsonButton.optString("postback_text");
                    presenter.generateButtonComment(jsonObject.optString("payload"), postBack, "button_postback_response");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        mode.getMenuInflater().inflate(R.menu.qiscus_comment_action, menu);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        menu.findItem(R.id.action_reply).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        menu.findItem(R.id.action_copy).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return true;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        onSelectedCommentsAction(mode, item, adapter.getSelectedComments());
        adapter.clearSelectedComments();
        return false;
    }

    private void onSelectedCommentsAction(ActionMode mode, MenuItem item, List<QiscusComment> selectedComments) {
        int i = item.getItemId();
        if (i == R.id.action_copy) {
            copyComments(selectedComments);
        } else if (i == R.id.action_reply) {
            if (selectedComments.size() > 0) {
                replyComment(selectedComments.get(0));
            }
        } else if (i == R.id.action_delete && selectedComments.size() > 0) {
            if (selectedComments.size() == 1) {
                presenter.deleteComment(selectedComments.get(0));
            } else {
                presenter.deleteComment(selectedComments);
            }
        }
        mode.finish();
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
        actionMode = null;
        adapter.clearSelectedComments();
    }

    protected void replyComment(QiscusComment qiscusComment) {
        try {
            repliedQiscusComment = qiscusComment;
            bindRepliedMessage(qiscusComment);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        KeyboardUtil.showKeyboard(this, mEditComment);
    }

    protected void copyComments(List<QiscusComment> selectedComments) {
        QiscusChatRoom chatRoom = presenter.getRoomData();
        if (roomMembers == null) {
            roomMembers = new HashMap<>();
            for (QiscusRoomMember member : chatRoom.getMember()) {
                roomMembers.put(member.getEmail(), member);
            }
        }
        String textCopied;
        if (selectedComments.size() == 1) {
            QiscusComment qiscusComment = selectedComments.get(0);
            textCopied = qiscusComment.isAttachment() ? qiscusComment.getAttachmentName() : qiscusComment.getMessage();
        } else {
            StringBuilder text = new StringBuilder();
            for (QiscusComment qiscusComment : selectedComments) {
                text.append(qiscusComment.getSender()).append(": ");
                text.append(qiscusComment.isAttachment() ? qiscusComment.getAttachmentName() :
                        qiscusComment.getMessage());
                text.append('\n');
            }
            textCopied = text.toString();
        }

        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText(getString(R.string.qiscus_chat_activity_label_clipboard), textCopied);
        if (clipboard != null) {
            clipboard.setPrimaryClip(clip);
        }
        Toast.makeText(this, getString(R.string.qiscus_copied_message, selectedComments.size()), Toast.LENGTH_SHORT).show();
    }

    private void bindRepliedMessage(QiscusComment qiscusComment) throws JSONException {
        mTextRepliedUser.setText(qiscusComment.getSender());
        switch (qiscusComment.getType()) {
            case IMAGE:
                mImageRepliedMessage.setVisibility(View.VISIBLE);
                File localPath = QiscusCore.getDataStore().getLocalPath(qiscusComment.getId());
                if (localPath == null) {
                    showBlurryImage(qiscusComment);
                } else {
                    showImage(localPath);
                }
                mTextRepliedMessage.setText(qiscusComment.getAttachmentName());
                break;
            case FILE:
                mImageRepliedMessage.setVisibility(View.VISIBLE);
                mImageRepliedMessage.setImageResource(R.drawable.ic_qiscus_file);
                mTextRepliedMessage.setText(qiscusComment.getAttachmentName());
                break;
            default:
                mImageRepliedMessage.setVisibility(View.GONE);
                mTextRepliedMessage.setText(qiscusComment.getMessage());
                break;
        }

        mLayoutRepliedMessage.setVisibility(View.VISIBLE);
    }

    private void showImage(File file) {
        Nirmana.getInstance().get()
                .setDefaultRequestOptions(new RequestOptions()
                        .centerCrop()
                        .dontAnimate()
                        .placeholder(R.drawable.qiscus_image_placeholder)
                        .error(R.drawable.qiscus_image_placeholder))
                .asBitmap()
                .load(file)
                .thumbnail(0.5f)
                .into(mImageRepliedMessage);
    }

    private void showBlurryImage(QiscusComment qiscusComment) {
        Nirmana.getInstance().get()
                .setDefaultRequestOptions(new RequestOptions()
                        .centerCrop()
                        .dontAnimate()
                        .placeholder(R.drawable.qiscus_image_placeholder)
                        .error(R.drawable.qiscus_image_placeholder))
                .asBitmap()
                .load(QiscusImageUtil.generateBlurryThumbnailUrl(qiscusComment.getAttachmentUri().toString()))
                .thumbnail(0.5f)
                .into(mImageRepliedMessage);
    }

    @Override
    public void onCommentSelected(List<QiscusComment> selectedComments) {
        boolean hasCheckedItems = selectedComments.size() > 0;
        if (hasCheckedItems && actionMode == null) {
            actionMode = startSupportActionMode(this);
        } else if (!hasCheckedItems && actionMode != null) {
            actionMode.finish();
        }

        if (actionMode != null) {
            actionMode.setTitle(getString(R.string.qiscus_selected_comment, selectedComments.size()));

            if (selectedComments.size() == 1 && selectedComments.get(0).getState() >= QiscusComment.STATE_ON_QISCUS) {

                actionMode.getMenu().findItem(R.id.action_reply).setVisible(true);

            } else {
                actionMode.getMenu().findItem(R.id.action_reply).setVisible(false);
            }

            if (onlyTextOrLinkType(selectedComments)) {
                actionMode.getMenu().findItem(R.id.action_copy).setVisible(true);
            } else {
                actionMode.getMenu().findItem(R.id.action_copy).setVisible(false);
            }

            actionMode.getMenu().findItem(R.id.action_delete).setVisible(true);
        }
    }

    private boolean onlyTextOrLinkType(List<QiscusComment> selectedComments) {
        for (QiscusComment selectedComment : selectedComments) {
            if (selectedComment.getType() != QiscusComment.Type.TEXT
                    && selectedComment.getType() != QiscusComment.Type.LINK
                    && selectedComment.getType() != QiscusComment.Type.REPLY
                    && selectedComment.getType() != QiscusComment.Type.CONTACT
                    && selectedComment.getType() != QiscusComment.Type.LOCATION) {
                return false;
            }
        }
        return true;
    }

    private class textWatcher implements TextWatcher {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (s.length() > 0) {
                setViewEnabled(mImageSend, true);
                changeImageSendTint(mImageSend, R.color.colorPrimary);
            } else {
                setViewEnabled(mImageSend, false);
                changeImageSendTint(mImageSend, R.color.qiscus_disabled);
            }
            presenter.setUserTyping(true);
        }

        @Override
        public void afterTextChanged(Editable s) {
            handler.removeCallbacks(input_finish_checker);

            handler.postDelayed(input_finish_checker, afterTextChangedDelay);
        }
    }
}